8in ascii default number system is decimal 0-9 , after 9 :;<=>? comes. let us say ascii is ascii_5_10 ( because 5X2=10 in decimal )
rustc number data types are decimal by default. so rust is rust_5_10
I am looking for c/c++/rust compiler for heksi_8_10. so rust_8_10.
// suppose programmer writes in rust_8_10 below line
let an_integer   D 5i20 B // sphiks annotation (actuslly rust_8_10 programmer will view this in roboto_heks_jk)
// then this line after compilation should create a variable :
//  an_integer of data type signed int of 32 bits ( because   20(heks) = 32(decimal)   
so how to do this.
[rust_8_10.png](/user_uploads/4715/FTzKeP94P9iGL4NzE9LTFEaA/rust_8_10.png)    

roboto_heks_jk font has all hexadecimal digits together   and   six   symbols  :;<=>?  afterwards  
[roboto_heks_jk.ttf](/user_uploads/4715/27rtW7ZcC5l5NL4dFPkuZKfy/roboto_heks_jk.ttf) 